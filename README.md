# Hungarian cinematics for Imperium Galactica II
This repository contains modified Hungarian cinematics for Imperium Galactica II. The original `.bik` files were specially re-encoded and converted to `.mp4` to work with the Steam version of the game.

## Installation
Firstly install the game like you normally would via Steam then copy (and replace if prompted) all the files in the game's `anim` folder with the contents of the `animations` folder of this repository.